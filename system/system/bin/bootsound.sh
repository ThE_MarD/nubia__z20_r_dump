#!/system/bin/sh

tinymix "TFA_CHIP_SELECTOR" 1
tinymix "QUAT_MI2S_RX Audio Mixer MultiMedia1" 1

echo PLAY bootup tone

/system/bin/tinyplay /system/media/audio/ui/bootup.wav

echo STOP bootup tone

tinymix "QUAT_MI2S_RX Audio Mixer MultiMedia1" 0
tinymix "TFA_CHIP_SELECTOR" 0

#disable
exit
